const express = require('express');

const productsRouter = require('./products.router');
const usersRouter = require('./users.router');
const categoriesRouter = require('./categories.router');

const api = '/api';
const v1 = '/v1';

function baseRouter(baseEndpoint, app){
  const router = express.Router();
  app.use(baseEndpoint, router);

  return router;

}

function routerApi(app){
  const router = baseRouter(api + v1, app);

  router.use('/products', productsRouter);
  router.use('/users', usersRouter);
  router.use('/categories', categoriesRouter);

}

module.exports = routerApi;
