const Joi = require('joi');

const id = Joi.string().uuid();
const name = Joi.string()
                  .min(3)
                .max(15);
const description = Joi.string()
                         .min(10)
                       .max(100);
const price = Joi.number()
                    .integer()
                  .min(10);
const image = Joi.string()
                 .uri();
const warehouse = Joi.string()
                        .min(3)
                     .max(50);
const qty = Joi.number()
                  .integer()
               .min(0)
                  .max(300);
const phoneNumber = Joi.string()
                     .min(3)
                       .max(50);
const stock = Joi.array().items(
  Joi.object({
    warehouse: warehouse,
    phoneNumber : phoneNumber,
    qty: qty

  })

);
const createProductSchema = Joi.object({
    name : name.required(),
    description : description.required(),
    price: price.required(),
    image: image.required(),
    isBlock : false,
    stock: stock

});
const updateProductSchema = Joi.object({
  name : name,
  description : description,
  price: price,
  image: image

});
const getProductSchema = Joi.object({
   id : id.required()

});

module.exports = { createProductSchema, updateProductSchema, getProductSchema}
