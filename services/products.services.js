const faker = require('faker');
const boom = require('@hapi/boom');

class ProductsService {
  constructor(){
    this.products = [];
    this.generate();

  }
  generate(){
    const productLimit = 10;

    for(let i = 0; i < productLimit; i++){
      this.createProduct(faker.datatype.uuid(), faker.commerce.productName(), faker.commerce.productDescription(), faker.commerce.price(),
                         faker.datatype.boolean(), "", this.getRandomArbitrary(0, 300),this.getRandomArbitrary(1, 5));

    }
    this.createAvailableProducts();
    this.createBlockedProducts();

  }
  async create(data){
    const newProduct = {
      id: faker.datatype.uuid(),
      ...data,
      isBlock: false

    }
    this.products.push(newProduct);
    return newProduct;

  }
  async find(){
    return this.products;

  }
  async findOne(id){
     const product = this.products.find(item => item.id === id);
     if(!product){
      throw boom.notFound('product not found');

     }
     if(product.isBlock){
       throw boom.conflict('product is blocked');

     }
     return product;

  }
  async update(id, changes){
    const index = this.products.findIndex(item => item.id === id);
    if(index === -1){
      throw boom.notFound('product not found')

    }
    const product = this.products[index];
    this.products[index] = {
      ...product,
      ...changes

    }
    return this.products[index];

  }
  async delete(id){
    const index = this.products.findIndex(item => item.id === id);
    if(index === -1){
      throw boom.notFound('product not found')

    }
    this.products.splice(index, 1);
    return { id };

  }
  createProduct(anId, aName, aDescription, aPrice, isBlocked, warehouse, qty, warehouseNumber){
    this.products.push({
      id: anId,
      name : aName,
      description : aDescription,
      price : parseInt(aPrice, 10),
      image: this.getRandomImage(),
      isBlock: isBlocked,
      stock: this.createStock(isBlocked, warehouse, qty, warehouseNumber)

    });

  }
  createAvailableProducts(){
    this.createProduct("a94fad5d-7421-4de3-bde0-f788f91ea796", "Chevrolet Corvette", "Favourite car in Street Rod 2", 1500, false, "React LLC", 159, 1);
    this.createProduct("a94fad5d-7421-4de3-bde0-f788f91ea797", "Shelby GT500", "King's Car in Street Rod 2", 1999, false, "TypeScript Group", 12, 2);
    this.createProduct("a94fad5d-7421-4de3-bde0-f788f91ea798", "Burgers Bungalow", "Race organicer for Mulholland Drive", 4500, false, "Python Inc", 299, 2);
    this.createProduct("a94fad5d-7421-4de3-bde0-f788f91ea799", "Bob's Drive-In", "Race place for Grudge Night", 4200, false, "Javascript and Sons", 45, 3);
    this.createProduct("a94fad5d-7421-4de3-bde0-f788f91ea800", "Prince of Persia", "A game by Brøderbund Software", 29, false, "NodeJS Ltd.", 101, 1);

  }
  createBlockedProducts(){
    this.createProduct("d30dcf76-45fc-4c5d-8e83-b0b7576df599", "Producto bloqueado 599", "Descripcion PB 599", 1, true);
    this.createProduct("d30dcf76-45fc-4c5d-8e83-b0b7576df598", "Producto bloqueado 598", "Descripcion PB 598", 1, true);
    this.createProduct("d30dcf76-45fc-4c5d-8e83-b0b7576df597", "Producto bloqueado 597", "Descripcion PB 597", 1, true);

  }
  getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;

  }
  getRandomImage(){
    let imageNumber = parseInt(this.getRandomArbitrary(0,1000));
    return 'https://picsum.photos/id/' + imageNumber + '/640/480'
  }
  createStock(isBlocked, warehouse, qty, warehouseNumber) {
    let stock = [];
    let j = 14;
    let k = 8;

    for (let i = 0; i < warehouseNumber; i++){
      if (!isBlocked){
        if (warehouse !== ""){
            stock.push({
              warehouse: warehouse + " branch office No. " + j,
              phoneNumber: faker.phone.phoneNumber('501-###-###'),
              qty : parseInt(qty + k, 10)


            })

        }else{
          stock.push({
              warehouse: faker.company.companyName(),
              phoneNumber: faker.phone.phoneNumber('501-###-###'),
              qty : parseInt(this.getRandomArbitrary(0, 300), 10)

            })

        }

      }else{
          stock;

      }
      j++;
      k = k + j;

    }


    return stock;

  }

}

module.exports = ProductsService
